import os

# App secret
SECRET_KEY = os.getenv("SECRET_KEY", "secret!@#")

# SQLAlchemy
SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI',
                                    "mysql+pymysql://root:password@127.0.0.1:3306/coding_games")
