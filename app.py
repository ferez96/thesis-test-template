import logging.config

logging.config.fileConfig("conf/logging.ini")


def create_app():
    _logger = logging.getLogger(__name__)

    from flask import Flask
    flask_app = Flask(__name__)
    flask_app.config.from_object('config')

    from flask_coding_game import CodingGameServer
    CodingGameServer(flask_app)

    flask_app.route("/", endpoint='index_view')(lambda: "Hello")

    # from .clients import TicTacToe, SamplePlayer

    return flask_app


if __name__ == "__main__":
    app = create_app()
    app.run()
