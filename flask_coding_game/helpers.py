"""
Helper functions, use python native libs. No-dependency
"""

import inspect
from typing import Callable


def inspect_function_name(f: Callable):
    return "{}:{}".format(inspect.getmodule(f).__name__, f)
