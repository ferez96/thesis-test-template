"""
CodingGameServer
"""

import logging
from flask_appbuilder import AppBuilder, SQLA, ModelView, expose
from flask import Flask

_logger = logging.getLogger(__name__)


class CodingGameServer(object):
    """Game control and flows"""

    def __init__(self, app: Flask = None):
        if app is not None:
            self.init_app(app)

        _logger.debug("CodingGameServer created")

    def init_app(self, app: Flask):
        """Init app"""
        _logger.debug(f"init_app {app}")

        db = SQLA(app)
        app_builder = AppBuilder(app, db.session)
        init_admin_sites(app_builder)

        self._init_extension(app)

    def _init_extension(self, app: Flask):
        app.flask_coding_game = self
        if not hasattr(app, "extensions"):
            app.extensions = {}
        app.extensions["flask_coding_game"] = self


"""
    Views
"""


def init_admin_sites(app_builder):
    pass
