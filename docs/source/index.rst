.. Flask Coding Game documentation master file, created by
   sphinx-quickstart on Sun May  3 13:39:30 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Flask Coding Game's documentation!
=============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Module flask_coding_game.core
=============================
.. automodule:: flask_coding_game.core
   :members:

Module flask_coding_game.common
===============================
.. automodule:: flask_coding_game.common
   :members:
