#!/usr/bin/env python
import os
import sys

project_root = os.path.abspath(__file__ + "/../../")
sys.path.insert(0, project_root)

from flask_coding_game.core import models
from sqlalchemy import create_engine
from config import SQLALCHEMY_DATABASE_URI

engine = create_engine(SQLALCHEMY_DATABASE_URI)
print("Migrate: ", engine)
models.Model.metadata.drop_all(engine)
models.Model.metadata.create_all(engine)
print("Finish")
